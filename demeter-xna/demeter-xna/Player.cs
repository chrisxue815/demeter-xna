﻿using BEPUphysics.Entities.Prefabs;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace demeter_xna
{
    public class Player : GameEntity
    {
        public Player()
        {
            const float width = 0.5f;
            const float height = 1.7f;
            const float length = 0.5f;

            var pos = new Vector3(100, 5, 0);

            LocalTransform = Matrix.CreateScale(new Vector3(width, height, length));

            ModelName = "cube";
            
            Body = new Box(pos, width, height, length, 50);
            //Body.BecomeKinematic();
        }

        public override void Update(GameTime gameTime)
        {
            const float speed = 50f;

            var timeDelta = (float)gameTime.ElapsedGameTime.TotalSeconds;
            var key = Keyboard.GetState();

            if (key.IsKeyDown(Keys.A))
            {
                Body.LinearVelocity = new Vector3(-speed, Body.LinearVelocity.Y, Body.LinearVelocity.Z);
                //Body.Position = Body.Position - new Vector3(speed * timeDelta, 0, 0);
            }
            else if (Body.LinearVelocity.X < 0)
            {
                Body.LinearVelocity = new Vector3(0, Body.LinearVelocity.Y, Body.LinearVelocity.Z);
            }

            if (key.IsKeyDown(Keys.D))
            {
                Body.LinearVelocity = new Vector3(speed, Body.LinearVelocity.Y, Body.LinearVelocity.Z);
                //Body.Position = Body.Position - new Vector3(speed * timeDelta, 0, 0);
            }
            else if (Body.LinearVelocity.X > 0)
            {
                Body.LinearVelocity = new Vector3(0, Body.LinearVelocity.Y, Body.LinearVelocity.Z);
            }

            if (UnderGround())
            {
                Body.Position = new Vector3(Body.Position.X, 0.1f + ((Box)Body).HalfHeight, Body.Position.Z);
                Body.LinearVelocity = new Vector3(Body.LinearVelocity.X, 0, Body.LinearVelocity.Z);
            }

            if (key.IsKeyDown(Keys.Space) && OnGround())
            {
                Body.LinearVelocity = new Vector3(Body.LinearVelocity.X, 5, Body.LinearVelocity.Z);
            }

            Body.Orientation = Quaternion.CreateFromYawPitchRoll(0, 0, 0);
        }

        public bool OnGround()
        {
            return Body.Position.Y - ((Box)Body).HalfHeight < 0.2f;
        }

        public bool UnderGround()
        {
            return Body.Position.Y - ((Box)Body).HalfHeight < 0.1f;
        }
    }
}
