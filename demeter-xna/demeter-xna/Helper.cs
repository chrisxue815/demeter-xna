﻿using BEPUphysics.Entities.Prefabs;

using Microsoft.Xna.Framework;

namespace demeter_xna
{
    public static class Helper
    {
        public static GameEntity CreateCube(Vector3 position, float width, float height, float length)
        {
            var theBox = new GameEntity();
            theBox.ModelName = "cube";
            theBox.LocalTransform = Matrix.CreateScale(new Vector3(width, height, length));
            theBox.Body = new Box(position, width, height, length, 1);

            return theBox;
        }
    }
}
