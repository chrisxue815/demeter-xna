using BEPUphysics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace demeter_xna
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        public Camera Camera { get; set; }
        public Player Player { get; set; }
        public Ground Ground { get; set; }

        public Space Space { get; set; }

        public static Game1 Instance { get; set; }

        public GraphicsDeviceManager Graphics { get; private set; }
        public SpriteBatch SpriteBatch { get; private set; }

        public Game1()
        {
            Instance = this;

            Graphics = new GraphicsDeviceManager(this);

            Graphics.PreferredBackBufferWidth = 1366;
            Graphics.PreferredBackBufferHeight = 768;
            Graphics.PreferMultiSampling = true;
            Graphics.SynchronizeWithVerticalRetrace = true;
            Graphics.ApplyChanges();

            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            Camera = new Camera();

            Space = new Space();
            Space.ForceUpdater.Gravity = new Vector3(0, -9.81f, 0);

            Player = new Player();
            Ground = new Ground();

            base.Initialize();
        }

        protected override void LoadContent()
        {
            SpriteBatch = new SpriteBatch(GraphicsDevice);

            Player.LoadContent();
            Ground.LoadContent();

            Space.Add(Player.Body);
            Space.Add(Ground.Body);
        }

        protected override void Update(GameTime gameTime)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            Space.Update();

            Player.Update(gameTime);
            Ground.Update(gameTime);

            Camera.Update(gameTime);

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            SpriteBatch.Begin();

            var state = new DepthStencilState();
            state.DepthBufferEnable = true;
            GraphicsDevice.DepthStencilState = state;

            Player.Draw(gameTime);
            Ground.Draw(gameTime);

            SpriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
