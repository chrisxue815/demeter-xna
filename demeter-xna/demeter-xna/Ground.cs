﻿using BEPUphysics.Entities.Prefabs;
using Microsoft.Xna.Framework;

namespace demeter_xna
{
    public class Ground : GameEntity
    {
        public Ground()
        {
            const float width = 1000f;
            const float height = 0.1f;
            const float length = 100f;

            var pos = new Vector3(width/2, -height/2, 0);

            LocalTransform = Matrix.CreateScale(new Vector3(width, height, length));

            ModelName = "cube";

            Body = new Box(pos, width, height, length, 1);
            Body.BecomeKinematic();
        }
    }
}
