﻿using System;
using BEPUphysics.Entities;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace demeter_xna
{
    public class GameEntity
    {
        // logical
        //public Vector3 Pos { get; set; }
        public Vector3 Look { get; set; }
        public Vector3 Up { get; set; }
        public Vector3 Right { get; set; }
        public Vector3 Basis { get; set; }

        public Vector3 Velocity { get; set; }

        // draw
        public string ModelName { get; set; }
        public Model Model { get; set; }
        public Vector3 DiffuseColor { get; set; }
        public Matrix LocalTransform { get; set; }

        // physics
        public Entity Body { get; set; }

        public GameEntity()
        {
            var random = new Random();
            DiffuseColor = new Vector3((float)random.NextDouble(), (float)random.NextDouble(), (float)random.NextDouble());
            Basis = new Vector3(0, 0, -1);
        }

        public virtual void LoadContent()
        {
            Model = Game1.Instance.Content.Load<Model>(ModelName);
        }

        public virtual void Update(GameTime gameTime)
        {
            
        }

        public virtual void Draw(GameTime gameTime)
        {
            if (Model != null)
            {
                foreach (var mesh in Model.Meshes)
                {
                    foreach (BasicEffect effect in mesh.Effects)
                    {
                        effect.EnableDefaultLighting();
                        effect.PreferPerPixelLighting = true;
                        effect.DiffuseColor = DiffuseColor;
                        effect.World = LocalTransform * Body.WorldTransform;
                        effect.Projection = Game1.Instance.Camera.Projection;
                        effect.View = Game1.Instance.Camera.View;
                    }

                    mesh.Draw();
                }
            }
        }
    }
}
