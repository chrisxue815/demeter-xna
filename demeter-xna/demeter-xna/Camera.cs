﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace demeter_xna
{
    public class Camera : GameEntity
    {
        public Matrix Projection { get; set; }
        public Matrix View { get; set; }

        public Camera()
        {
            Up = new Vector3(0, 1, 0);
        }

        public override void Update(GameTime gameTime)
        {
            var playerPos = Game1.Instance.Player.Body.Position;

            var pos = new Vector3(playerPos.X + 3, 3, 10);
            var tar = new Vector3(pos.X, pos.Y, -5);

            View = Matrix.CreateLookAt(pos, tar, Up);
            Projection = Matrix.CreatePerspectiveFieldOfView(
                MathHelper.PiOver4, Game1.Instance.Graphics.GraphicsDevice.Viewport.AspectRatio, 1.0f, 10000.0f);
        }

        public override void Draw(GameTime gameTime)
        {
        }
    }
}
